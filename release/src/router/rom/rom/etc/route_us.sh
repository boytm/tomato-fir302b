#/bin/sh

action=$1

vpn=$2
net=`nvram get wan_iface`
lan=`nvram get lan_ifname`

modprobe ipt_set
table=" table 8 "

# 465 TCP Gmail SMTP port
# 993 TCP Gmail IMAP port
# 5228 TCP/UDP Google Play port
tcp_dest_ports="80,443,993,5228"
udp_dest_ports="5228"

case $action in
add)
	ip route replace $table default dev $vpn
	# delete again before add?
	iptables -t mangle -A PREROUTING -i ${lan} -p tcp ! -d 192.168.0.0/16 -m multiport --dports ${tcp_dest_ports} -j MARK --set-mark 0xffff
	iptables -t mangle -A PREROUTING -i ${lan} -p udp ! -d 192.168.0.0/16 -m multiport --dports ${udp_dest_ports} -j MARK --set-mark 0xffff
	ip rule add fwmark 0xffff $table priority 1

        echo 0 > /proc/sys/net/ipv4/conf/${vpn}/rp_filter
;;
del)
        echo 1 > /proc/sys/net/ipv4/conf/${vpn}/rp_filter

	ip rule del fwmark 0xffff $table priority 1
	iptables -t mangle -D PREROUTING -i ${lan} -p tcp ! -d 192.168.0.0/16 -m multiport --dports ${tcp_dest_ports} -j MARK --set-mark 0xffff 
	iptables -t mangle -D PREROUTING -i ${lan} -p udp ! -d 192.168.0.0/16 -m multiport --dports ${udp_dest_ports} -j MARK --set-mark 0xffff
	ip route replace $table default dev $net
;;
*)
	echo "Unkown action: $action" >&2
	exit 1
;;
esac

# public dns server
ip route $action 8.8.8.8/32 dev $vpn
ip route $action 8.8.4.4/32 dev $vpn
ip route $action 208.67.220.220/32 dev $vpn
ip route $action 208.67.222.222/32 dev $vpn

# route table for marked packet
#ip route replace default dev $vpn
ip route $action $table 0.0.0.0/7 dev $net
ip route $action $table 14.0.0.0/8 dev $net
ip route $action $table 27.0.0.0/8 dev $net
ip route $action $table 36.0.0.0/8 dev $net
ip route $action $table 39.0.0.0/8 dev $net
ip route $action $table 42.0.0.0/7 dev $net
ip route $action $table 45.64.0.0/10 dev $net
ip route $action $table 49.0.0.0/8 dev $net
ip route $action $table 54.222.0.0/15 dev $net
ip route $action $table 58.0.0.0/7 dev $net
ip route $action $table 60.0.0.0/7 dev $net
ip route $action $table 60.254.0.0/16 dev $vpn
ip route $action $table 91.232.0.0/13 dev $net
ip route $action $table 101.0.0.0/8 dev $net
ip route $action $table 102.0.0.0/7 dev $net
ip route $action $table 103.246.192.0/18 dev $vpn
ip route $action $table 106.0.0.0/8 dev $net
ip route $action $table 110.0.0.0/7 dev $net
ip route $action $table 112.0.0.0/4 dev $net
ip route $action $table 113.28.0.0/15 dev $vpn
ip route $action $table 139.0.0.0/12 dev $net
ip route $action $table 139.128.0.0/15 dev $net
ip route $action $table 139.148.0.0/15 dev $net
ip route $action $table 139.152.0.0/13 dev $net
ip route $action $table 139.170.0.0/16 dev $net
ip route $action $table 139.176.0.0/16 dev $net
ip route $action $table 139.183.0.0/16 dev $net
ip route $action $table 139.186.0.0/16 dev $net
ip route $action $table 139.188.0.0/14 dev $net
ip route $action $table 139.192.0.0/11 dev $net
ip route $action $table 139.224.0.0/16 dev $net
ip route $action $table 139.226.0.0/15 dev $net
ip route $action $table 140.75.0.0/16 dev $net
ip route $action $table 140.143.0.0/16 dev $net
ip route $action $table 140.205.0.0/16 dev $net
ip route $action $table 140.206.0.0/15 dev $net
ip route $action $table 140.210.0.0/16 dev $net
ip route $action $table 140.224.0.0/16 dev $net
ip route $action $table 140.237.0.0/16 dev $net
ip route $action $table 140.240.0.0/16 dev $net
ip route $action $table 140.243.0.0/16 dev $net
ip route $action $table 140.246.0.0/16 dev $net
ip route $action $table 140.249.0.0/16 dev $net
ip route $action $table 140.250.0.0/16 dev $net
ip route $action $table 140.255.0.0/16 dev $net
ip route $action $table 144.0.0.0/15 dev $net
ip route $action $table 144.6.0.0/15 dev $net
ip route $action $table 144.12.0.0/16 dev $net
ip route $action $table 144.52.0.0/14 dev $net
ip route $action $table 144.122.0.0/15 dev $net
ip route $action $table 144.255.0.0/16 dev $net
ip route $action $table 150.0.0.0/10 dev $net
ip route $action $table 150.115.0.0/16 dev $net
ip route $action $table 150.121.0.0/16 dev $net
ip route $action $table 150.122.0.0/16 dev $net
ip route $action $table 150.128.0.0/15 dev $net
ip route $action $table 150.138.0.0/15 dev $net
ip route $action $table 150.223.0.0/16 dev $net
ip route $action $table 150.242.0.0/16 dev $net
ip route $action $table 150.254.0.0/15 dev $net
ip route $action $table 152.104.0.0/13 dev $net
ip route $action $table 153.0.0.0/15 dev $net
ip route $action $table 153.3.0.0/16 dev $net
ip route $action $table 153.34.0.0/15 dev $net
ip route $action $table 153.36.0.0/15 dev $net
ip route $action $table 153.96.0.0/14 dev $net
ip route $action $table 153.100.0.0/15 dev $net
ip route $action $table 153.118.0.0/15 dev $net
ip route $action $table 157.0.0.0/12 dev $net
ip route $action $table 157.16.0.0/14 dev $net
ip route $action $table 157.61.0.0/16 dev $net
ip route $action $table 157.122.0.0/16 dev $net
ip route $action $table 157.148.0.0/16 dev $net
ip route $action $table 157.156.0.0/14 dev $net
ip route $action $table 157.255.0.0/16 dev $net
ip route $action $table 159.226.0.0/16 dev $net
ip route $action $table 161.207.0.0/16 dev $net
ip route $action $table 162.105.0.0/16 dev $net
ip route $action $table 163.0.0.0/15 dev $net
ip route $action $table 163.44.0.0/14 dev $net
ip route $action $table 163.48.0.0/12 dev $net
ip route $action $table 163.125.0.0/16 dev $net
ip route $action $table 163.136.0.0/13 dev $net
ip route $action $table 163.177.0.0/16 dev $net
ip route $action $table 163.178.0.0/15 dev $net
ip route $action $table 163.204.0.0/16 dev $net
ip route $action $table 166.110.0.0/15 dev $net
ip route $action $table 167.139.0.0/16 dev $net
ip route $action $table 167.189.0.0/16 dev $net
ip route $action $table 168.160.0.0/16 dev $net
ip route $action $table 171.0.0.0/9 dev $net
ip route $action $table 171.64.0.0/12 dev $vpn
ip route $action $table 171.208.0.0/12 dev $net
ip route $action $table 175.0.0.0/8 dev $net
ip route $action $table 180.0.0.0/6 dev $net
ip route $action $table 192.124.154.0/23 dev $net
ip route $action $table 192.188.168.0/22 dev $net
ip route $action $table 202.0.0.0/7 dev $net
ip route $action $table 202.72.96.0/19 dev $vpn
ip route $action $table 203.144.0.0/18 dev $vpn
ip route $action $table 203.187.128.0/19 dev $vpn
ip route $action $table 210.0.0.0/7 dev $net
ip route $action $table 218.0.0.0/7 dev $net
ip route $action $table 220.0.0.0/6 dev $net

ip route flush cache
